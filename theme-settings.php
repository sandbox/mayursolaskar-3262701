<?php

/**
 * @file
 * Function to support medical_website settings.
 */

use Drupal\Core\From;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implement settings
 */
function medical_website_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['medical_website_settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => new TranslatableMarkup('medical_website Settings'),
  ];

  $form['basic'] = [
      '#type' => 'details',
      '#group' => 'medical_website_settings',
      '#title' => new TranslatableMarkup('mysettings'),
  ];

  $form['basic']['type_theme'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('select mode'),
      '#default_value' => theme_get_setting('type_theme'),
      '#options' => [
          'light' => new TranslatableMarkup('Light mode'),
          'dark' => new TranslatableMarkup('Dark mode'),
      ],
    ];

    $form['footer'] = [
        '#type' => 'details',
        '#group' => 'medical_website_settings',
        '#title' => new TranslatableMarkup('footerlayout'),
    ];

    $form['footer']['type_footer'] = [
        '#type' => 'select',
        '#title' => new TranslatableMarkup('select footer layout'),
        '#default_value' => theme_get_setting('type_footer'),
        '#options' => [
            'first' => new TranslatableMarkup('Footer first'),
            'second' => new TranslatableMarkup('Footer second'),
            'third' => new TranslatableMarkup('Footer third'),
            'fourth' => new TranslatableMarkup('Footer fourth'),
        ],
    ];

    $form['content'] = [
        '#type' => 'details',
        '#group' => 'medical_website_settings',
        '#title' => new TranslatableMarkup('Content'),
    ];
    $form['content']['401_content'] = [
        '#type' => 'text_format',
        '#format' => 'full_html',
        '#title' => new TranslatableMarkup('401 Content'),
        '#default_value' => theme_get_setting('401_content')['value'],
    ];

    $form['content']['403_content'] = [
        '#type' => 'text_format',
        '#format' => 'full_html',
        '#title' => new TranslatableMarkup('403 Content'),
        '#default_value' => theme_get_setting('403_content')['value'],
    ];

    $form['content']['404_content'] = [
        '#type' => 'text_format',
        '#format' => 'full_html',
        '#title' => new TranslatableMarkup('404 Content'),
        '#default_value' => theme_get_setting('404_content')['value'],
    ];
}